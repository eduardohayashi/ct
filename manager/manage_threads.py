from queue import Queue
from threading import Thread
import pandas as pd
from scrap.extract import Extract
from scrap.interact import Interact


class ManageThreads(object):

    def __init__(self):
        self.open_threads = 0
        self.queue = Queue()
        self.frames = []

    def start_process(self, limit, amount_save=None):
        """
        Control amount started threads to read cities informations
        :param limit: amount of threads allowed together
        :param amount_save: amount of cities to be read and save
        :return: None
        """
        pages = self.__get_pages_id(amount_save)

        for page in pages:
            if self.open_threads >= limit or self.open_threads >= 10:
                self.queue.get()

            Thread(target=Extract().start, args=(page, self.finish_read)).start()
            self.open_threads += 1

        while self.open_threads > 0:
            pass

        self.__save_csv()

    def __get_pages_id(self, amount_save):
        """
        Get array with ids to read
        :param amount_save: length of ids array
        :return:
        """
        try:
            df_id = self.__read_csv_ids()
            if not df_id.empty:
                ids = df_id.values.tolist()
            else:
                print('Extracting cities ids')
                ids = Interact().get_pages()

            if amount_save:
                return ids[:amount_save+1]
            else:
                return ids

        except Exception as exc:
            print('Error getting pages id: ', str(exc))

    def __read_csv_ids(self):
        """
        Try read saved ids file
        :return: pandas DataFrame with cities ids
        """
        try:
            return pd.read_csv('city_ids.csv')
        except:
            return pd.DataFrame()

    def __save_csv(self):
        """
        Save on disk the csv with all readed cities
        :return: None
        """
        climate_df = pd.concat(self.frames)
        climate_df.to_csv('climates.csv', index=False, encoding='utf-8')

    def finish_read(self, result):
        """
        Callback when finished to read a page
        :param result: pandas DataFrame with the page information
        :return: None
        """
        self.frames.append(result)
        self.queue.put('Finished')
        self.open_threads -= 1
