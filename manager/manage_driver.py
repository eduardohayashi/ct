from selenium import webdriver
from pyvirtualdisplay import Display
import settings


class ManageDriver(object):

    def get_driver(self):
        """
        Wait the driver start
        :return: Firefox webdriver
        """
        display = Display(visible=0)
        display.start()

        driver = None
        while not driver:
            driver = self.__initate_driver()

        return driver

    def __initate_driver(self):
        """
        Start new Firefox webdriver
        :return: Firefox webdriver
        """
        try:
            profile = webdriver.FirefoxProfile(settings.PROFILE_PATH)
            driver = webdriver.Firefox(profile)
            driver.set_window_size(400, 400)
            return driver
        except Exception as exc:
            print('Error starting driver: ', str(exc))
