import requests
from bs4 import BeautifulSoup
import pandas as pd


class Extract(object):

    def start(self, page_number, callback):
        """
        Read page and return pandas DataFrame
        :param page_number: page to mount link
        :param callback: callback function when finish read
        :return: None
        """
        try:
            url = 'https://www.climatempo.com.br/climatologia/' + str(self.__get_page(page_number)) + '/fakecity'
            print('Reading page ', page_number)
            page = requests.get(url, timeout=60)
            self.soup_page = BeautifulSoup(page.content, 'html.parser')

            callback(self.__get_information())
        except Exception as exc:
            print('Error on start process: ', str(exc))

    def __get_information(self):
        """
        Mount json with information readed
        :return: pandas DataFrame with informations readed
        """
        location = self.__get_location()
        table_rows = self.soup_page.find('table', {'role': 'grid'}).findChild('tbody').findChildren('tr')
        year = []

        for row in table_rows:
            coluns = row.findChildren('td')
            year.append(
                {'month': coluns[0].get_text(), 'minimum': coluns[1].get_text(), 'maximum': coluns[2].get_text(),
                 'precipitation': coluns[3].get_text(), 'city': location['city'], 'state': location['state']})

        return pd.DataFrame.from_dict(year)

    def __get_location(self):
        """
        Split the label to get locate information
        :return: json with city and state readed
        """
        location = self.soup_page.find('p', {'data-reveal-id': 'geolocation'}).get_text()
        index_scape = location.index('-')
        city = location[:index_scape].strip().lstrip()
        state = location[index_scape:].strip().lstrip().replace('-', '')

        return {'city': city, 'state': state}

    def __get_page(self, page_number):
        """
        Format page number to string
        :param page_number: page number
        :return: formated page number
        """
        if type(page_number) is list:
            return str(page_number[0])
        else:
            return str(page_number)
