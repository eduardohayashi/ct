import time
import pandas as pd
from selenium.webdriver.support.ui import Select
from manager.manage_driver import ManageDriver


class Interact(object):

    def __init__(self):
        self.ids = []
        self.driver = ManageDriver().get_driver()

    def get_pages(self):
        """
        Get page to read all cities ids
        :return: array with cities ids
        """
        self.driver.get('https://www.climatempo.com.br/climatologia/553/saojosedoscampos-sp')
        self.driver.find_element_by_xpath('/html/body/div[6]/div[1]/div[2]/div[2]/div/div[2]/div[1]/div[2]/p[1]/span[2]').click()
        self.__get_states_and_cities()

        self.driver.quit()
        pd.DataFrame(self.ids).to_csv('city_ids.csv', index=False, encoding='utf-8')
        return self.ids

    def __get_states_and_cities(self):
        """
        Select state peer state to read cities ids
        :return:None
        """
        state_combo = self.driver.find_element_by_id('sel-state-geo')
        while not state_combo.is_enabled():
            pass

        index = 0
        while index < 27:
            Select(self.driver.find_element_by_id('sel-state-geo')).select_by_index(index)
            time.sleep(2)
            self.__get_cities()
            index += 1

    def __get_cities(self):
        """
        Get all cities ids
        :return: None
        """
        try:
            ids = []
            cities = self.driver.find_element_by_id('sel-city-geo').find_elements_by_tag_name('option')

            for city in cities:
                ids.append(city.get_attribute('value'))

            self.ids += ids
        except:
            self.__get_cities()
