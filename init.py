import settings
from datetime import datetime
from manager.manage_threads import ManageThreads

print('Starting extractor: ', datetime.now())
ManageThreads().start_process(settings.THREAD_LIMITS, settings.AMOUNT_SAVE)
print('Extractor finalized: ', datetime.now())