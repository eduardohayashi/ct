# Serasa - Clima Tempo [CT]

Scrapy all cities from climatempo.com.br and save some informations on "csv" format.

## How to execute

* Clone the repository https://bitbucket.org/bianca_berdugo/ct
* Install the dependencies list with pip install
```
pip install requirements.txt
```
* If the city_ids.csv file doens't exists, you will need install firefox and xvfb, create new profile and configure.
```
apt-get install firefox
apt-get install xvfb
```
    1. On console type "firefox -P"
    2. Create a new profile
    3. Acess page "https://www.climatempo.com.br/climatologia/550/saojoaquimdabarra-sp"
    4. Allow location
    5. Go to preferences and set block geo location to that site
    6. Configure profile path on settings.py

* Configure the threads limit on settings.py (max limit = 10)
* Configure the amount cities will be saved on settings.py
```
AMOUNT_SAVE = 100  -> to save 100 cities
AMOUNT_SAVE = None -> to save all cities
```
* Run the init.py script to start
```
python init.py
``` 
* Get the result in "climates.csv" file 

### Prerequisites

* Python 3
* Firefox 57

## Author

*Bianca Vicensotti Berdugo*
